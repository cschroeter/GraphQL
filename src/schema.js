import { GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLList, GraphQLNonNull } from 'graphql';
import axios from 'axios';


const CompanyType = new GraphQLObjectType({
  name: 'CompanyType',
  fields: () => ({
    id: {
      type: GraphQLInt
    },
    name: {
      type: GraphQLString
    },
    description: {
      type: GraphQLString
    },
    users: {
      type: new GraphQLList(UserType),
      resolve({id}) {
        console.log('Fetching users with company id', id);
        return axios.get(`http://localhost:4001/companies/${id}/users`)
          .then(response => response.data);
      }
    }
  })
});

const UserType = new GraphQLObjectType({
  name: 'UserType',
  fields: () => ({
    id: {
      type: GraphQLInt
    },
    firstName: {
      type: GraphQLString
    },
    age: {
      type: GraphQLInt
    },
    company: {
      type: CompanyType,
      resolve({id, companyId}) {
        console.log('Fetching company for user with id', id);
        return axios.get(`http://localhost:4001/companies/${companyId}`)
          .then(response => response.data);
      }
    }
  })
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    users: {
      type: new GraphQLList(UserType),
      resolve() {
        console.log('Fetching all users');
        return axios.get(`http://localhost:4001/users`)
          .then(response => response.data);
      }
    },
    user: {
      type: UserType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parentValue, {id}) {
        console.log('Fetching user with id', id);
        return axios.get(`http://localhost:4001/users/${id}`)
          .then(response => response.data);
      }
    },
    companies: {
      type: CompanyType,
      args: {
        id: {
          type: GraphQLInt
        }
      },
      resolve(parentvalue, {id}) {
        console.log('Fetching company with id', id);
        return axios.get(`http://localhost:4001/companies/${id}`)
          .then(response => response.data);
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addUser: {
      type: UserType,
      args: {
        firstName: {
          type: new GraphQLNonNull(GraphQLString)
        },
        age: {
          type: GraphQLInt
        },
        companyId: {
          type: GraphQLInt
        }
      },
      resolve(parentValue, {firstName, age, companyId}) {
        console.log('Creating user..');
        return axios.post(`http://localhost:4001/users`, {firstName, age, companyId})
          .then(response => response.data);
      }
    },
    updateUser: {
      type: UserType,
      args: {
        id: {
          type: new GraphQLNonNull(GraphQLInt)
        },
        firstName: {
          type: GraphQLString
        },
        age: {
          type: GraphQLInt
        },
        companyId: {
          type: GraphQLInt
        }
      },
      resolve(parentValue, user) {
        console.log('Updating user with id', user.id);
        return axios.patch(`http://localhost:4001/users/${user.id}`, user)
          .then(response => response.data);
      }
    },
    deleteUser: {
      type: UserType,
      args: {
        id: {
          type: new GraphQLNonNull(GraphQLInt)
        }
      },
      resolve(parentValue, {id}) {
        console.log('Deleting user with id', id);
        return axios.delete(`http://localhost:4001/users/${id}`)
          .then(response => response.data);
      }
    }
  }
});

const Schema = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation
});

export default Schema;
